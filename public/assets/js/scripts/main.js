var posicionaImagemMedia = function(indice){
	var offset = parseInt($('#ampliadas .viewport-ampliadas a').eq(indice).position().left) * -1;	
	$('#ampliadas .viewport-ampliadas').css('left', offset);
}

$('document').ready( function(){

	if(jQuery().cycle){
		$('#slides').cycle({
			timeout : 9000,
			speed   : 6000
		});
	}

	$('aside nav ul').addClass('collapse');
	$('#toggle-menu').click( function(e){
		e.preventDefault();
		$('aside nav ul').toggleClass('collapse');
	})

	$('form').submit( function(e){
		var erros_validacao = false;

		// Verifica campos obrigatórios
		$("[required]").each( function(){
			if($(this).val() == ''){
				alert($(this).attr('data-required-text'));
				e.preventDefault();
				erros_validacao = true;
				return false;
			}
		});

		if(erros_validacao) return false;		
	});

	if($('.envio.sucesso').length){
		setTimeout( function(){
			$('.envio.sucesso').fadeOut();
		}, 7000);
	}

	$('.abre-sub').click( function(e){
		var prox = $(this).next('.submenu')
		$('.submenu').addClass('escondido');
		prox.toggleClass('escondido');
	});

	if(jQuery().fancybox){
		if(jQuery(window).width() > 767){
			$('.abre-galeria-clippings').fancybox({
				titleShow : false,
				overlayColor : '#63584D',
				overlayOpacity : 0.99,
				opacity : true,
				padding : 0,
				autoScale : false,
				onComplete : function(){
					$('#fancybox-overlay').css('position', 'fixed');
				}
			});
		}else{
			$('.abre-galeria-clippings').click( function(e){
				e.preventDefault();
				window.location.href = 'clippings/noshadow/'+$(this).attr('data-id');
			})
		}
	}

	if(jQuery().imagesLoaded){

		$('#thumbs .viewport-thumbs a').click( function(e){
			e.preventDefault();
		});

		if($('#thumbs a').length > 17){
			$('#thumbs .next-btn').css('display', 'inline-block');
		}else{
			$('#thumbs .viewport-thumbs').css('width', parseInt($('#thumbs a').length) * 55);
		}

		if(window.location.hash){
			var splithash = window.location.hash.split('=');
			if(splithash[1] == 'Forn'){
				if($('.fornecedores-open').length){
					setTimeout( function(){
						$('.fornecedores-open').click();					
					}, 300);
				}				
			}else{
				$('#thumbs .viewport-thumbs a').eq(splithash[1]).addClass('ativo');
				posicionaImagemMedia(splithash[1]);
			}
		}

		$('#ampliadas .viewport-ampliadas a').fancybox({
			type : 'image',
			titleShow : false,
			overlayColor : '#63584D',
			overlayOpacity : 0.99,
			opacity : true,
			padding : 0			
		});

		$('#thumbs .next-btn').click( function(e){
			e.preventDefault();
			if(!$(this).hasClass('animando')){
				$(this).addClass('animando');
				var deslocamento_atual = parseInt($('#thumbs .viewport-thumbs .move-handle').css('left'));
				var maximo = (parseInt($('#thumbs a').length) - 18) * - 55;
				console.log(maximo)
				if(deslocamento_atual - 55 >= maximo){
					$('#thumbs .viewport-thumbs .move-handle').css('left', deslocamento_atual - 55)
					setTimeout( function(){
						$('#thumbs .next-btn').removeClass('animando');
					}, 300);
				}
				if(deslocamento_atual - 55 == maximo){
					$('#thumbs .next-btn').fadeOut();
				}
				$('#thumbs').addClass('comprev');				
			}
		});

		$('#thumbs .prev-btn').click( function(e){
			e.preventDefault();
			if(!$(this).hasClass('animando')){
				$(this).addClass('animando');
				var deslocamento_atual = parseInt($('#thumbs .viewport-thumbs .move-handle').css('left'));
				var maximo = 0;
				if(deslocamento_atual + 55 <= maximo){
					$('#thumbs .viewport-thumbs .move-handle').css('left', deslocamento_atual + 55)
					setTimeout( function(){
						$('#thumbs .prev-btn').removeClass('animando');

						if(deslocamento_atual + 55 == maximo){
							$('#thumbs').removeClass('comprev');
						}
					}, 300);
				}

				$('#thumbs .next-btn').css('display', 'inline-block');
			}
		});

		$('.fornecedores-open').click( function(e){
			e.preventDefault();
			var offset = parseInt($('#ampliadas .viewport-ampliadas .fornecedores-card').position().left) * -1;	
			$('#thumbs a.ativo').removeClass('ativo');
			$(this).addClass('ativo');
			$('#ampliadas .viewport-ampliadas').css('left', offset);
			window.location.hash = 'mostraFoto=Forn';
		});

		imagesLoaded( $('.detalhe-projeto'), function( instance ) {
			
			$('#thumbs .viewport-thumbs a').unbind('click');

			$('#thumbs .viewport-thumbs a').click( function(e){
				e.preventDefault();
				$('.fornecedores-open').removeClass('ativo');
				$('#thumbs a.ativo').removeClass('ativo');
				$(this).addClass('ativo');
				window.location.hash = 'mostraFoto='+$(this).index();
				posicionaImagemMedia($(this).index());
			});

		});
	}

});