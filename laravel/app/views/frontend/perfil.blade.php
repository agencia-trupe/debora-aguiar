@section('conteudo')

	<div class="pure-g-r perfil-grid">
		
		<div class="pure-u-1-3">
			@if($perfil)
				<div class="imagem-debora" style="background-image:url('assets/images/perfil/{{$perfil->imagem}}');"></div>
			@endif
		</div>

		<div class="pure-u-1-3">
			<div class="texto-perfil">
				@if($perfil)
					{{ $perfil->{Concat::sufixo('perfil')} }}
				@endif
			</div>
		</div>

		<div class="pure-u-1-3 fundo-marrom">
			<div class="texto-metodologia">
				@if($perfil)
					{{ $perfil->{Concat::sufixo('metodologia')} }}
				@endif				
			</div>
		</div>

	</div>

@stop