<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Senhas precisam tem no mínimo 6 caracteres e devem coincidir com a confirmação.",

	"user"     => "Não foi encontrado um usuário com este e-mail.",

	"token"    => "Este token é inválido.",

	"sent" => "Lembrete de senha enviado!",

);