<?php

return array(

	'home_titulo' => 'Página Inicial',
	'home' => 'HOME',

	'perfil_titulo' => 'Perfil',
	'perfil' => 'PERFIL',

	'portfolio_titulo' => 'Portfolio',
	'portfolio' => 'PORTFOLIO',

	'clientes_titulo' => 'Nossos Clientes',
	'clientes' => 'CLIENTES',

	'parceiros_titulo' => 'Nossos Parceiros',
	'parceiros' => 'PARCEIROS',

	'clipping_titulo' => 'Clipping',
	'clipping' => 'CLIPPING',

	'contato_titulo' => 'Contato',
	'contato' => 'CONTATO',

	'area_restrita_titulo' => 'área restrita',
	'area_restrita' => 'área restrita',

	'versao_ingles_titulo' => 'english version',
	'versao_ingles' => 'english',

	'versao_portugues_titulo' => 'versão em português',
	'versao_portugues' => 'português',

	'assinatura' => 'Debora Aguiar &bull; Todos os direitos reservados',
	'criacao_sites_titulo' => 'Criação de Sites : Trupe Agência Criativa',
	'criacao_sites' => 'Criação de Sites',
	'trupe' => 'Trupe Agência Criativa',
	
	'clipping_impresso_titulo' => 'Clippings Impressos',
	'clipping_impresso' => 'IMPRESSO',
	'clipping_digital_titulo' => 'Clippings Digitais',
	'clipping_digital' => 'DIGITAL',

	'toggle' => 'Mostrar/Esconder Menu'
);