<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Contato;

class ContatoController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('painel.contato.index')->with('contato', Contato::orderBy('id')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.contato.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Contato;
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Informações de contato criadas com sucesso.');

		return Redirect::route('painel.contato.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.contato.edit')->with('contato', Contato::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Contato::find($id);
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Informações de contato alteradas com sucesso.');

		return Redirect::route('painel.contato.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Contato::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Informações de contato removidas com sucesso.');

		return Redirect::route('painel.contato.index');
	}

}