@section('conteudo')

	@if(sizeof($projetos) > 0)
		<ul class="lista-projetos">
			@foreach($projetos as $projeto)
				@if($projeto->projetos_categorias_id != '42')
					<li>
						<a href="portfolio/{{ $projeto->categoria->{Concat::sufixo('slug')} }}/{{ $projeto->{Concat::sufixo('slug')} }}" title="{{ $projeto->{Concat::sufixo('titulo')} }}">
							<img src="assets/images/portfolio/thumbs/{{ $projeto->capa }}" alt="{{ $projeto->{Concat::sufixo('titulo')} }}">
						</a>
					</li>
				@else
					<li>
						<a href="{{$projeto->link}}" target="_blank" title="{{ $projeto->{Concat::sufixo('titulo')} }}">
							<img src="assets/images/portfolio/thumbs/{{ $projeto->capa }}" alt="{{ $projeto->{Concat::sufixo('titulo')} }}">
						</a>
					</li>
				@endif
			@endforeach			
		</ul>
	@else
		<h1 class="sem-resultado padded">{{ Lang::get('geral.nenhum_projeto')}}</h1>
	@endif

@stop