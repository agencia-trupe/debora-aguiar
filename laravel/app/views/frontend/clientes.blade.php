@section('conteudo')

	@if(sizeof($clientes) > 0)

		<div class="lista-clientes">

			@foreach($clientes as $cliente)
				
				@if($cliente->link != '')
					<a href="{{ $cliente->link }}" target="_blank" title="{{ $cliente->{Concat::sufixo('titulo')} }}" class="cliente">
						<img src="assets/images/{{$imageDir}}/{{$cliente->imagem}}" alt="{{ $cliente->{Concat::sufixo('titulo')} }}">
					</a>
				@else
					<div title="{{ $cliente->{Concat::sufixo('titulo')} }}" class="cliente">
						<img src="assets/images/{{$imageDir}}/{{$cliente->imagem}}" alt="{{ $cliente->{Concat::sufixo('titulo')} }}">
					</div>
				@endif

			@endforeach			

		</div>

	@else
		
		@if($imageDir == 'clientes')
			<h1 class="sem-resultado padded">{{ Lang::get('geral.nenhum_cliente') }}</h1>
		@else
			<h1 class="sem-resultado padded">{{ Lang::get('geral.nenhum_parceiro') }}</h1>
		@endif

	@endif

@stop