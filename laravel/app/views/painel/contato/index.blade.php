@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Contato
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed">

          		<thead>
            		<tr>
              			<th>Texto</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($contato as $registro)

                	<tr class="tr-row">
                  		<td>
                        PT: {{ Str::words($registro->texto_pt, 10) }}
                        <br><br>
                        EN: {{ Str::words($registro->texto_en, 10) }}
                      </td>                  		
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.contato.edit', $registro->id ) }}" class="btn btn-primary">editar</a>
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop