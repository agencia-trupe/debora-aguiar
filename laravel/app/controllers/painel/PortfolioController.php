<?php

namespace Painel;

use View, Input, Str, Session, DB, Redirect, File, Imagine;

use Categoria, Portfolio, PortfolioImagens;

class PortfolioController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catId = Input::get('catId');

		if($catId){
			$portfolio = Portfolio::orderBy('ordem')->where('projetos_categorias_id', '=', $catId)->get();

			$categoria = Categoria::find($catId);

			if(!is_null($categoria))
				$catTit = $categoria->titulo_pt;
			else
				$catTit = false;
			
		}else{
			$portfolio = Portfolio::orderBy('ordem')->get();
			$catTit = false;
		}

		$this->layout->content = View::make('painel.portfolio.index')->with('portfolio', $portfolio)
																	 ->with('listaCategorias', Categoria::orderBy('ordem')->get())
																	 ->with('catId', $catId)
																	 ->with('catTit', $catTit);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.portfolio.form')->with('categorias', Categoria::orderBy('ordem')->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Portfolio;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/portfolio/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/portfolio/', $filename);
			$object->capa = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(700,700);
			$sizeThumb = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/portfolio/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/portfolio/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/portfolio/thumbs/'.$filename, array('quality' => 100));
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->fornecedores = Input::get('fornecedores');
		$object->projetos_categorias_id = Input::get('projetos_categorias_id');
		$object->link = Input::get('swf');
		$object->save();
		
		$object->slug_pt = Str::slug($object->id.'_'.$object->titulo_pt);
		$object->slug_en = Str::slug($object->id.'_'.$object->titulo_en);

		$object->ordem = (DB::table('projetos')->where('projetos_categorias_id','=',Input::get('projetos_categorias_id'))->max('ordem')) + 1;

		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Projeto criado com sucesso.');

		return Redirect::route('painel.portfolio.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.portfolio.edit')->with('portfolio', Portfolio::find($id))->with('categorias', Categoria::orderBy('ordem')->get());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Portfolio::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/portfolio/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/portfolio/', $filename);
			$object->capa = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(700,700);
			$sizeThumb = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/portfolio/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/portfolio/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/portfolio/thumbs/'.$filename, array('quality' => 100));
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->fornecedores = Input::get('fornecedores');
		$object->projetos_categorias_id = Input::get('projetos_categorias_id');
		$object->link = Input::get('swf');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Projeto alterado com sucesso.');

		return Redirect::route('painel.portfolio.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Portfolio::find($id)->delete();
		PortfolioImagens::where('projetos_id', '=', $id)->delete();
		
		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Projeto removido com sucesso.');

		return Redirect::route('painel.portfolio.index');
	}
}