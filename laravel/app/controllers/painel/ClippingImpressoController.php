<?php

namespace Painel;

use View, Input, Str, Session, Redirect, File, Imagine, ClippingImpresso, ClippingImpressoImagem;

class ClippingImpressoController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('painel.clippingImpresso.index')->with('clippings', ClippingImpresso::orderBy('data', 'DESC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.clippingImpresso.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ClippingImpresso;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/clippings/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/clippings/', $filename);
			$object->capa = $filename;
			
			ini_set('memory_limit','256M');
			$imagine = new Imagine\Gd\Imagine();
			
			$imagine->open('assets/images/clippings/'.$filename)
					->thumbnail(new Imagine\Image\Box(180,240), Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND, TRUE)
					->save('assets/images/clippings/thumbs/'.$filename, array('quality' => 100));
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->data = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",Input::get('data'));
		$object->save();

		$object->slug_pt = Str::slug($object->id.'_'.$object->titulo_pt);
		$object->slug_en = Str::slug($object->id.'_'.$object->titulo_en);
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Clipping criado com sucesso.');

		return Redirect::route('painel.clippingImpresso.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.clippingImpresso.edit')->with('clipping', ClippingImpresso::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ClippingImpresso::find($id);
		
		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/clippings/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/clippings/', $filename);
			$object->capa = $filename;

			ini_set('memory_limit','256M');
			$imagine = new Imagine\Gd\Imagine();
			
			$imagine->open('assets/images/clippings/'.$filename)
					->thumbnail(new Imagine\Image\Box(180,240), Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND, TRUE)
					->save('assets/images/clippings/thumbs/'.$filename, array('quality' => 100));
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->data = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",Input::get('data'));
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Clipping alterado com sucesso.');

		return Redirect::route('painel.clippingImpresso.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		ClippingImpresso::find($id)->delete();
		ClippingImpressoImagem::where('clipping_impresso_id', '=', $id)->delete();
		
		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Clipping removido com sucesso.');

		return Redirect::route('painel.clippingImpresso.index');
	}

}