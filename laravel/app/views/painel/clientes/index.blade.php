@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Clientes <a href="{{ URL::route('painel.clientes.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Cliente</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="clientes">

          		<thead>
            		<tr>
                    <th style="width:40px;">ordenar</th>
              			<th>Título</th>
              			<th>Imagem</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($clientes as $cliente)

                	<tr class="tr-row" id="row_{{$cliente->id}}">
                      <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
                  		<td>{{ $cliente->titulo_pt }}</td>
                  		<td><img src="assets/images/clientes/{{ $cliente->imagem }}"></td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.clientes.edit', $cliente->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.clientes.destroy', $cliente->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.clientes.destroy', $cliente->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop