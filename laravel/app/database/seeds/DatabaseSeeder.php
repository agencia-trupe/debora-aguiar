<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PerfilSeeder');
		$this->call('ContatoSeeder');
		$this->call('ProjetosCategoriasSeeder');
		$this->call('UsersSeeder');
	}

}