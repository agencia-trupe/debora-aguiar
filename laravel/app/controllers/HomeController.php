<?php

use \Banner;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'frontend.template';

	public function index()
	{
		$this->layout->with('load_js', 'plugins/cycle');

		$this->layout->content = View::make('frontend.home')->with('banners', Banner::orderBy('ordem', 'ASC')->get());
	}

}