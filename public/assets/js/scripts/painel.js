var Admin = {

  toggleLoginRecovery: function(){
    var is_login_visible = $('#modal-login').is(':visible');
    (is_login_visible ? $('#modal-login') : $('#modal-recovery')).slideUp(300, function(){
      (is_login_visible ? $('#modal-recovery') : $('#modal-login')).slideDown(300, function(){
        $(this).find('input:text:first').focus();
      });
    });
  },

  scrollTop: function(){
    $('html, body').animate({
      'scrollTop' : 0
    }, 300);
  },

  voltar: function(){
    window.history.back();
  },

  mostraAlerta: function(){
    var alerta = $('.alert');
    if(!alerta.hasClass('dont-dismiss')){
      setTimeout( function(){
        alerta.slideUp(400, function(){
          alerta.remove();
        })
      }, 10000);
    }
  },

  mostraBotaoScroll: function(){
    var hasVScroll = parseInt($('body').css('height')) > window.innerHeight;
    if(!hasVScroll)
      $('#footer .inner .container .right a').hide();
  },

  filtraCidades: function(id_estado, target){
    $.getJSON('ajax/pegarCidades/'+id_estado, function(resposta){
      target.html("<option value=''>Carregando Cidades...</option>");
      var itens = "<option value=''>Todas as Cidades do Estado</option>";
      resposta.map( function(a, b){
        itens += "<option value='"+a.id+"'>"+a.nome+"</option>";
      });
      target.html(itens);
      if(target.attr('data-cidade'))
        target.val(target.attr('data-cidade'))
    });
  }

};

var optionStore;

$(function(){

  $('.toggle-login-recovery').click(function(e){
    Admin.toggleLoginRecovery();
    e.preventDefault();
  });

  $('#footer .inner .container .right a').click( function(e){
    Admin.scrollTop();
    e.preventDefault();
  });

  $('.btn-move').click( function(e){
    e.preventDefault();
  });

  $('.btn-delete').click( function(e){
    e.preventDefault();
    var form = $(this).closest('form');
    bootbox.confirm("Deseja Excluir o Registro?", function(result){
      if(result)
        form.submit();
      else
        $(this).modal('hide');
    });
  });

  $('.btn-voltar').click( function(e){
    e.preventDefault();
    Admin.voltar();
  });

  if($('.alert').length){
    Admin.mostraAlerta();
  }

  $("table.table-sortable tbody").sortable({
    update : function () {
      serial = [];
      tabela = $('table.table-sortable').attr('data-tabela');
      $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
          serial.push(elm.id.split('_')[1])
      });
      $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
    },
    helper: function(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    },
    handle : $('.btn-move')
  }).disableSelection();

  $('.datepicker').datepicker();

  $('.monthpicker').monthpicker();

  Admin.mostraBotaoScroll();

  tinyMCE.init({
    language : "pt",
    mode : "specific_textareas",
    editor_selector : "imagem",
    theme : "advanced",
    theme_advanced_buttons1 : "styleselect,separator,bold,italic,underline,separator,link,unlink,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,image,separator,fullscreen",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_statusbar_location : 'bottom',
    theme_advanced_path : false,
    plugins: "paste,advimage,fullscreen",
    paste_text_sticky : true,
    theme_advanced_resizing : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
            ed.getDoc().body.style.fontSize = '16px';
            ed.pasteAsPlainText = true;
        });
    },
    height: 450,
    content_css : BASE+"/assets/css/painel/css/tinymceFull.css?nocache=" + new Date().getTime(),
    style_formats: [
      {title: 'Título', block: 'h3', attributes : { 'class' : 'preto semmargem-b semmargem-t'}},
      {title: 'Sub Vermelho', block: 'p', attributes : { 'class' : 'vermelho bold semmargem-b semmargem-t'}},
      {title: 'Sub Preto', block: 'p', attributes : { 'class' : 'preto bold semmargem-b semmargem-t'}},
      {title: 'Parágrafo Comum', block: 'p', attributes : { 'class' : 'preto semmargem-b semmargem-t'}},
    ]
  });

  tinyMCE.init({
    language : "pt",
    mode : "specific_textareas",
    editor_selector : "blog",
    theme : "advanced",
    theme_advanced_buttons1 : "bold,italic,underline,separator,link,unlink,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,jbimages,separator,fullscreen",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_statusbar_location : 'bottom',
    theme_advanced_path : false,
    plugins: "paste,jbimages,fullscreen",
    paste_text_sticky : true,
    theme_advanced_resizing : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
            ed.getDoc().body.style.fontSize = '16px';
            ed.pasteAsPlainText = true;
        });
    },
    height: 450    
  });

  tinyMCE.init({
    language : "pt",
    mode : "specific_textareas",
    editor_selector : "olho",
    theme : "advanced",
    theme_advanced_buttons1 : "bold,italic,underline",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_statusbar_location : 'bottom',
    theme_advanced_path : false,
    plugins: "paste",
    paste_text_sticky : true,
    theme_advanced_resizing : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
            ed.getDoc().body.style.fontSize = '16px';
            ed.getDoc().body.classList.add("olho");
            ed.pasteAsPlainText = true;
        });
    },
    theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
    font_size_style_values : "10px,12px,13px,14px,16px,18px",
    content_css : BASE+"/assets/css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

  tinyMCE.init({
    language : "pt",
    mode : "specific_textareas",
    editor_selector : "inscricoes",
    theme : "advanced",
    theme_advanced_buttons1 : "styleselect,separator,bold,italic,underline",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_statusbar_location : 'bottom',
    theme_advanced_path : false,
    plugins: "paste",
    paste_text_sticky : true,
    theme_advanced_resizing : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
            ed.getDoc().body.style.fontSize = '16px';
            ed.getDoc().body.classList.add("inscricoes");
            ed.pasteAsPlainText = true;
        });
    },
    height : 200,
    content_css : BASE+"/assets/css/painel/css/tinymce.css?nocache=" + new Date().getTime(),
    style_formats: [
      {title: 'Vermelho', block: 'p', attributes : { 'class' : 'vermelho'}},
      {title: 'Preto', block: 'p', attributes : { 'class' : 'preto'}},
    ]
  });

  tinyMCE.init({
    language : "pt",
    mode : "specific_textareas",
    editor_selector : "completo",
    theme : "advanced",
    theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,fontsizeselect",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_blockformats : "h1, h2, p",
    theme_advanced_toolbar_location : "top",
    theme_advanced_statusbar_location : 'bottom',
    theme_advanced_path : false,
    plugins: "paste",
    paste_text_sticky : true,
    theme_advanced_resizing : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
            ed.getDoc().body.style.fontSize = '14px';
            ed.pasteAsPlainText = true;
        });
    },
    theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
    font_size_style_values : "10px,12px,13px,14px,16px,18px",
    content_css : BASE+"/assets/css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

  tinyMCE.init({
    language : "pt",
    mode : "specific_textareas",
    editor_selector : "basico",
    theme : "advanced",
    theme_advanced_buttons1 : "bold,italic,underline,bullist,link,unlink,fontsizeselect",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_statusbar_location : 'bottom',
    theme_advanced_path : false,
    plugins: "paste",
    paste_text_sticky : true,
    theme_advanced_resizing : true,
    setup : function(ed) {
        ed.onInit.add(function(ed) {
            ed.getDoc().body.style.fontSize = '14px';
            ed.pasteAsPlainText = true;
        });
    },
    theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
    font_size_style_values : "10px,12px,13px,14px,16px,18px",
    content_css : BASE+"/assets/css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

  $('#form-create-usuario').submit( function(){
    if($('#senha').val() == '' || $('#conf-senha').val() == ''){
      bootbox.alert('Informe a senha e a confirmação de senha corretamente.');
      $('#senha').focus();
      return false;
    }
    if($('#senha').val() != $('#conf-senha').val()){
      bootbox.alert('As senhas informadas não conferem.');
      $('#senha').focus();
      return false;
    }
  });

  $('#form-edit-usuario').submit( function(){
    if( ($('#senha').val()!= '') &&  ($('#senha').val() != $('#conf-senha').val())){
      bootbox.alert('As senhas informadas não conferem.');
      $('#senha').focus();
      return false;
    }
  });

  $('#form-create-banner').submit( function(){
    if($('input[name=imagem]').val() == ''){
      bootbox.alert('Selecione a imagem do Banner.');
      $('input[name=imagem]').focus();
      return false;
    }
    if($('input[name=titulo]').val() == ''){
      bootbox.alert('Informe o título do Banner.');
      $('input[name=titulo]').focus();
      return false;
    }
    if($('input[name=link]').val() == ''){
      bootbox.alert('Informe o destino do Link.');
      $('input[name=link]').focus();
      return false;
    }
  });

  $('#form-edit-banner').submit( function(){
    if($('input[name=titulo]').val() == ''){
      bootbox.alert('Informe o título do Banner.');
      $('input[name=titulo]').focus();
      return false;
    }
    if($('input[name=link]').val() == ''){
      bootbox.alert('Informe o destino do Link.');
      $('input[name=link]').focus();
      return false;
    }
  });

  var formulariosComValidacao = $('#form-create-linhas ,#form-edit-linhas, #form-create-novidade, #form-edit-novidade, #form-create-produto, #form-edit-produto, #form-create-representantes, #form-edit-representantes, #form-edit-tipo');

  formulariosComValidacao.submit( function(e){
    var inputs = $('input, select');
    inputs.each( function(){
      if($(this).attr('required') && $(this).val() == ''){
        bootbox.alert($(this).attr('required-message'));
        e.preventDefault();
      }
    });
  });

  $('.seleciona_estado').live('change', function(){
    var id_estado = $(this).val();
    if(id_estado)
      Admin.filtraCidades(id_estado, $(this).parent().next().find('.seleciona_cidade'));
  });

  $('.seleciona_estado').each( function(){
    if($(this).val() != ''){
      var alvo = $(this).parent().next().find('.seleciona_cidade');
      Admin.filtraCidades($(this).val(), alvo);
    }
  });

  var clone_labels = $('.labels-originais').html() + '<br>';

  $('#acrescentar-cidade').click( function(e){
    e.preventDefault();
    $('.cidades-forms').append(clone_labels);
  });

  $('.btn-remover').live('click', function(e){
    e.preventDefault();
    $(this).parent().prev().remove();
    $(this).parent().prev().remove();
    $(this).parent().remove();
  });

  $('.cadernos-only').hide();
  $('.sel-miolos').hide();

  if($('#sel-tipo-produto').val() == 11)
    $('.cadernos-only').show();

  if($('#sel-tipo-produto').val() == 11 || $('#sel-tipo-produto').val() == 12)
    $('.sel-miolos').show();

  $('#sel-tipo-produto').change( function(){
    if($(this).val() == 11){
      $('.cadernos-only').show();
      $('.sel-miolos').show();
    }else if($(this).val() == 12){
      $('.cadernos-only').hide();
      $('.sel-miolos').show();
    }else{
      $('.cadernos-only').hide();
      $('.sel-miolos').hide();
    }
  });

  $('.abre-lista-encontrar').click( function(e){
    e.preventDefault();
    var destino = $(this).attr('href');
    $.fancybox({
      'type' : 'iframe',
      'href' : destino,
      'width' : '100%',
      'height' : 600
    });
  });

  $('.disable-after-submit').click( function(e){
    var botao = $(this);
    setTimeout( function(){
      botao.attr('disabled', 'disabled');
    }, 300);    
  });

  $('#form-create-portfolio select, #form-edit-portfolio select').change( function(){
    if($(this).val() == '41'){
      $('#forn-txt').show();
    }else if($(this).val() == '42'){
      $('#swf-txt').show();
    }else{
      $('#swf-txt').hide();
      $('#forn-txt').hide();
    }
  });

  $('#add-img-proj').click( function(e){
    e.preventDefault();
    $('#form-imagem').toggle();
  });

});
