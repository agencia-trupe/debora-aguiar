<?php

use \ClippingImpresso, \ClippingImpressoImagem;

class ClippingsImpressosController extends BaseController {

	protected $layout = 'frontend.template';

	public function index($id = false)
	{
		$this->layout->with('load_js', array('plugins/fancybox'))
					 ->with('load_css', array('fancybox/fancybox'));

		$clippings = ClippingImpresso::orderBy('data', 'desc')->get();

		foreach ($clippings as $key => $value) {
			
			$primeira = ClippingImpressoImagem::orderBy('ordem', 'asc')->where('clipping_impresso_id', $value->id)->first();

			if(sizeof($primeira) == 0)
				$value->inicial = $value->capa;
			else
				$value->inicial = $primeira->imagem;			
		}

		$this->layout->content = View::make('frontend.clippings.impressos')->with('clippings', $clippings);
	}

	public function noShadow($id){
		$this->layout->content = View::make('frontend.clippings.noShadow')->with('clipping', ClippingImpresso::find($id));
	}

}