<?php
/**
*	Classe para inserir os Assets (js, less e css) no html
*	@author Bruno Monteiro
*	@since 21/08/2013
*/
class Assets
{
	// Strings de Retorno
	private static $css;
	private static $js;

	// Diretórios padrão
	private static $css_dir = 'assets/css/';
	private static $js_dir = 'assets/js/';

	// Sufixos dos arquivos
	private static $css_sufixo = '';
	private static $js_sufixo = '';

	// Coloca ou não um timestamp no path para evitar o cache
	private static $css_no_cache = TRUE;
	private static $js_no_cache = TRUE;

	/**
	*	Prepara a string para incluir CSS
	*	@access public
	*	@param array (lista de nomes de arquivos) || string (nome do arquivo)
	*	@return string
	*/
	public static function CSS($include, $no_cache = TRUE){
		self::$css = "";
		self::$css_no_cache = $no_cache;

		if(is_array($include))
			self::processa_CSS_array($include);
		else
			self::processa_CSS_string($include);

		return self::$css;
	}

	/**
	*	Itera o array para inclusão de arquivos
	*	@access private
	*	@param array
	*	@return void
	*/
	private static function processa_CSS_array($array){
		foreach($array as $item)
			self::processa_CSS_string($item);
	}

	/**
	*	Gera a String de inclusão para o CSS
	*	@access private
	*	@param string
	*	@return void
	*/
	private static function processa_CSS_string($string){

		$arquivo_css_sufixo = self::$css_dir.$string.self::$css_sufixo.'.css';
		$arquivo_css = self::$css_dir.$string.'.css';
		$arquivo_less = self::$css_dir.$string.'.less';

		if(file_exists($arquivo_css_sufixo))
		{
			// Existe o arquivo com sufixo extensão .css. Incluí-lo!
			if(self::$css_no_cache)
				$arquivo_css_sufixo .= "?nocache=".time();

			self::$css .= "<link rel='stylesheet' href='".$arquivo_css_sufixo."'>";

		}elseif(file_exists($arquivo_css))
		{
			// Existe o arquivo sem sufixo extensão .css. Incluí-lo!
			if(self::$css_no_cache)
				$arquivo_css .= "?nocache=".time();

			self::$css .= "<link rel='stylesheet' href='".$arquivo_css."'>";
		}elseif(file_exists($arquivo_less))
		{
			// Existe o arquivo com a extensão .less. Incluí-lo!
			if(self::$css_no_cache)
				$arquivo_less .= "?nocache=".time();

			self::$css .= "<link rel='stylesheet/less' type='text/css' href='".$arquivo_less."'>";
		}else
			Log::info('Erro ao carregar arquivo CSS ('.$string.')');
	}

	/**
	*	Prepara a string para incluir JavaScript
	*	@access public
	*	@param array (lista de nomes de arquivos) || string (nome do arquivo)
	*	@return string
	*/
	static function JS($include, $no_cache = TRUE){
		self::$js = "";
		self::$js_no_cache = $no_cache;

		if(is_array($include))
			self::processa_JS_array($include);
		else
			self::processa_JS_string($include);

		return self::$js;
	}

	/**
	*	Itera o array para inclusão de arquivos
	*	@access private
	*	@param array
	*	@return void
	*/
	private static function processa_JS_array($array){
		foreach($array as $item)
			self::processa_JS_string($item);
	}

	/**
	*	Gera a String de inclusão para o JS
	*	@access private
	*	@param string
	*	@return void
	*/
	private static function processa_JS_string($string){

		$arquivo_js = self::$js_dir.$string.'.js';
		$arquivo_js_sufixo = self::$js_dir.$string.self::$js_sufixo.'.js';

		if(file_exists($arquivo_js_sufixo))
		{
			// Existe o arquivo com sufixo e extensão .js. Incluí-lo!
			if(self::$js_no_cache)
				$arquivo_js_sufixo .= "?nocache=".time();

			self::$js .= "<script src='".$arquivo_js_sufixo."' type='text/javascript'></script>";
		}elseif(file_exists($arquivo_js))
		{
			// Existe o arquivo com sufixo e extensão .js. Incluí-lo!
			if(self::$js_no_cache)
				$arquivo_js .= "?nocache=".time();

			self::$js .= "<script src='".$arquivo_js."' type='text/javascript'></script>";
		}else
			Log::info('Erro ao carregar arquivo JS ('.$string.')');
	}
}