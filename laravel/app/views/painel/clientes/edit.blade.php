@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Cliente
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.clientes.update', $cliente->id), 'files' => true,'method' => 'put', 'id' => 'form-alter-cliente') ) }}

			<label>Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
			<input type="text" name="titulo_pt" required required-message="Informe o título do cliente!" class="input-xxlarge" value="{{$cliente->titulo_pt}}"></label>

			<label>Título - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
			<input type="text" name="titulo_en" required required-message="Informe o título do cliente!" class="input-xxlarge" value="{{$cliente->titulo_en}}"></label>
			
			<label>Imagem<br>
				@if($cliente->imagem)
					<img src="assets/images/clientes/{{ $cliente->imagem }}" style="width:200px;"><br>				
				@endif
				<input type="file" name="imagem">
			</label>			

			<label>Link<br>
			<input type="text" name="link" value="{{$cliente->link}}">
			</label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop