@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Imagens do projeto "{{ $portfolio->titulo_pt }}"
    	</h2>
      <a href="#" class="btn btn-success" id="add-img-proj" style="clear:both; float:none;"><i class="icon-plus-sign"></i> Adicionar Imagem</a>
      <a href="painel/portfolio" title="Voltar" class="btn" style="float:none;">Voltar</a>
      
      <form action="{{URL::route('painel.imagens.store', array('projetos_id' => $portfolio->id))}}" method="post" enctype="multipart/form-data" id="form-imagem" style="display:none; padding:10px; margin:5px;">
        <h4>Envio de Imagem</h4>
        <label>
          <input type="file" name="imagem" required required-message="Selecione uma imagem para o envio!">
        </label>
        {{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
      </form>

  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="projetos_imagens">

          		<thead>
            		<tr>
                    <th>ordenar</th>
              			<th>Imagem</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($imagens as $imagem)

                	<tr class="tr-row" id="row_{{$imagem->id}}">
                      <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
                  		<td><img src="assets/images/portfolio/thumbs/{{$imagem->imagem}}" style="max-width:120px"></td>
                  		<td class="crud-actions">
                    		 {{ Form::open(array('route' => array('painel.imagens.destroy', $imagem->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.imagens.destroy', $imagem->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop