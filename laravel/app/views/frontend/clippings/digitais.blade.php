@section('conteudo')
	
	@if(sizeof($clippings) > 0)
		<div class="pad-clips">
			<ul class="lista-clippings-digitais">
			@foreach($clippings as $clipping)
				<li>
					<a href="{{ $clipping->link }}" target="_blank" title="{{ $clipping->{Concat::sufixo('titulo')} }}">
						<?php
							$meses = array(
								'01' => 'janeiro',
								'02' => 'fevereiro',
								'03' => 'março',
								'04' => 'abril',
								'05' => 'maio',
								'06' => 'junho',
								'07' => 'julho',
								'08' => 'agosto',
								'09' => 'setembro',
								'10' => 'outubro',
								'11' => 'novembro',
								'12' => 'dezembro'
							);
							list($ano,$mes,$dia) = explode('-', $clipping->data);
							$data_formatada = $meses[$mes].' '.$ano;
						?>
						<span>{{ $clipping->{Concat::sufixo('titulo')} }}</span> &bull; {{ $data_formatada }}<br>
						{{ $clipping->{Concat::sufixo('descricao') } }}
					</a>
				</li>
			@endforeach
			</ul>
		</div>
	@else
		<h1 class="sem-resultado padded">{{ Lang::get('geral.nenhum_clipping')}}</h1>
	@endif

@stop