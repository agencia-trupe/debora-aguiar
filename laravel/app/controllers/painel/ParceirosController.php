<?php

namespace Painel;

use View, Input, Str, File, Session, Imagine, Redirect, Parceiro;

class ParceirosController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('painel.parceiros.index')->with('parceiros', Parceiro::orderBy('ordem')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.parceiros.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Parceiro;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/parceiros/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/parceiros/', $filename);
			$object->imagem = $filename;

			ini_set('memory_limit','256M');

			$imagine = new Imagine\Gd\Imagine();
			$resizer = new \Rtablada\Images\ResizeAndPad($imagine);
			$image = $imagine->open('assets/images/parceiros/'.$filename);
			$resizer->setSize(220, 160)->apply($image)->save('assets/images/parceiros/'.$filename);				
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->link = Input::get('link');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Parceiro inserido com sucesso.');

		return Redirect::route('painel.parceiros.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.parceiros.edit')->with('parceiro', Parceiro::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Parceiro::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/parceiros/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/parceiros/', $filename);
			$object->imagem = $filename;

			ini_set('memory_limit','256M');

			$imagine = new Imagine\Gd\Imagine();
			$resizer = new \Rtablada\Images\ResizeAndPad($imagine);
			$image = $imagine->open('assets/images/parceiros/'.$filename);
			$resizer->setSize(220, 160)->apply($image)->save('assets/images/parceiros/'.$filename);						
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->link = Input::get('link');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Parceiro alterado com sucesso.');

		return Redirect::route('painel.parceiros.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Parceiro::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Parceiro removido com sucesso.');

		return Redirect::route('painel.parceiros.index');
	}

}