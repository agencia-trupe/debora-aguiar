<?php

use \Parceiro;

class ParceirosController extends BaseController {

	protected $layout = 'frontend.template';

	public function index()
	{
		$this->layout->content = View::make('frontend.clientes')->with('clientes', Parceiro::orderBy('ordem', 'ASC')->get())
																->with('imageDir', 'parceiros');
	}

}