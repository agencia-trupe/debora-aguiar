<?php

use \Contato;

class ContatoController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'frontend.template';

	public function index()
	{
		$this->layout->content = View::make('frontend.contato')->with('contato', Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('contato@deboraaguiar.com', 'Contato - Debora Aguiar')
			    		->subject('Contato via site')
			    		->bcc('bruno@trupe.net')
			    		->replyTo($data['email'], $data['nome']);
			});
			Session::flash('envio', true);
		}else{
			Session::flash('envio', false);
		}

		return Redirect::to('contato');		
	}

}