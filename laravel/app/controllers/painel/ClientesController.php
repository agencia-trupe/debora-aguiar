<?php

namespace Painel;

use View, Input, Str, File, Session, Imagine, Redirect, Cliente;

class ClientesController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('painel.clientes.index')->with('clientes', Cliente::orderBy('ordem')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.clientes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Cliente;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/clientes/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/clientes/', $filename);
			$object->imagem = $filename;

			ini_set('memory_limit','256M');

			$imagine = new Imagine\Gd\Imagine();
			$resizer = new \Rtablada\Images\ResizeAndPad($imagine);
			$image = $imagine->open('assets/images/clientes/'.$filename);
			$resizer->setSize(220, 160)->apply($image)->save('assets/images/clientes/'.$filename);			
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->link = Input::get('link');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cliente inserido com sucesso.');

		return Redirect::route('painel.clientes.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.clientes.edit')->with('cliente', Cliente::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Cliente::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/clientes/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/clientes/', $filename);
			$object->imagem = $filename;

			ini_set('memory_limit','256M');

			$imagine = new Imagine\Gd\Imagine();
			$resizer = new \Rtablada\Images\ResizeAndPad($imagine);
			$image = $imagine->open('assets/images/clientes/'.$filename);
			$resizer->setSize(220, 160)->apply($image)->save('assets/images/clientes/'.$filename, array('quality' => 100));				
		}

		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->link = Input::get('link');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cliente alterado com sucesso.');

		return Redirect::route('painel.clientes.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Cliente::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cliente removido com sucesso.');

		return Redirect::route('painel.clientes.index');
	}

}