<?php

use \Portfolio, \PortfolioImagens;

class PortfolioController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'frontend.template';

	public function index($categoria = false, $projeto = false)
	{
		// Se não há Categoria, usa Arquitetura como padrão
		if(!$categoria)
			$categoria = Concat::sufixo('') == '_pt' ? 'arquitetura'  : 'architecture';		

		$objCategoria = Categoria::where(Concat::sufixo('slug'), '=', $categoria)->first();

		if(sizeof($objCategoria) > 0)
			$this->layout->with('categoriaSelecionada', $objCategoria->{Concat::sufixo('slug')});
		else
			App::abort('404');

		// Procura pelo Projeto da url
		$objProjeto = Portfolio::where(Concat::sufixo('slug'), '=', $projeto)->first();

		// Listagem de Categoria
		// Sem Projeto especificado, listar todos os Projetos da Categoria
		if(is_null($objProjeto))
		{
			$this->layout->content = View::make('frontend.portfolio.index')->with('projetos', $objCategoria->projetos)
																 	   	   ->with('objCategoria', $objCategoria);			
		}else
		// Detalhe do Projeto
		// Com o Projeto especificado, listar o Projeto e as Imagens dele
		{
			$libs_js = array(
				'plugins/eventEmitter/EventEmitter.min',
				'plugins/eventie/eventie',
				'plugins/imagesloaded/imagesloaded',
				'plugins/fancybox'
			);
			$this->layout->with('load_js', $libs_js)
						 ->with('load_css', array('fancybox/fancybox'));

			$this->layout->content = View::make('frontend.portfolio.view')->with('projeto', $objProjeto)
																	 	  ->with('imagens', $objProjeto->imagens)
																	 	  ->with('objCategoria', $objCategoria)
																	 	  ->with('nav', $this->navegacao($objCategoria, $objProjeto));
		}		
	}

	private function navegacao($objCategoria, $objProjeto){
		$arr = array(
			'anterior' => array('existe' => false, 'url' => ''),
			'proxima' => array('existe' => false, 'url' => ''),
		);

		$anterior = Portfolio::orderBy('ordem', 'DESC')->where('projetos_categorias_id', '=', $objProjeto->projetos_categorias_id)->where('ordem', '<', $objProjeto->ordem)->first();
		if(!is_null($anterior)){
			$arr['anterior'] = array('existe' => true, 'url' => 'portfolio/'.$objCategoria->{Concat::sufixo('slug')}.'/'.$anterior->{Concat::sufixo('slug')});
		}

		$proxima = Portfolio::orderBy('ordem', 'ASC')->where('projetos_categorias_id', '=', $objProjeto->projetos_categorias_id)->where('ordem', '>', $objProjeto->ordem)->first();
		if(!is_null($proxima)){
			$arr['proxima'] = array('existe' => true, 'url' => 'portfolio/'.$objCategoria->{Concat::sufixo('slug')}.'/'.$proxima->{Concat::sufixo('slug')});
		}

		return $arr;
	}

}