@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Banners <a href="{{ URL::route('painel.banners.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Banner</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="banners">

          		<thead>
            		<tr>
                    <th style="width:40px;">Ordenar</th>
                    <th>Imagem</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($banners as $banner)

                	<tr class="tr-row" id="row_{{ $banner->id }}">
                      <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
                      <td><img src="assets/images/home/thumbs/{{ $banner->imagem }}"></td>
                  		<td class="crud-actions">
                    		 {{ Form::open(array('route' => array('painel.banners.destroy', $banner->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.banners.destroy', $banner->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop