@section('conteudo')

	@if(sizeof($clippings) > 0)
		<div class="pad-clips">
			<ul class="lista-clippings-impressos">
			@foreach($clippings as $clipping)
				<li>
					<a href="assets/images/clippings/{{$clipping->inicial}}" rel="galeria-{{$clipping->id}}" class="abre-galeria-clippings" target="_blank" title="{{ $clipping->{Concat::sufixo('titulo')} }}" data-id="{{$clipping->id}}">
						<?php
							$meses = array(
								'01' => 'janeiro',
								'02' => 'fevereiro',
								'03' => 'março',
								'04' => 'abril',
								'05' => 'maio',
								'06' => 'junho',
								'07' => 'julho',
								'08' => 'agosto',
								'09' => 'setembro',
								'10' => 'outubro',
								'11' => 'novembro',
								'12' => 'dezembro'
							);
							list($ano,$mes,$dia) = explode('-', $clipping->data);
							$data_formatada = $meses[$mes].' '.$ano;
						?>
						<img src="assets/images/clippings/thumbs/{{$clipping->capa}}" alt="{{ $clipping->{Concat::sufixo('titulo')} }}">
						<div class="linha">
							{{ $clipping->{Concat::sufixo('titulo')} }}
						</div>
						{{ $data_formatada }}
					</a>
					@if($clipping->imagens)
						@foreach($clipping->imagens as $c => $imagem)
							@if($c > 0)
								<a href="assets/images/clippings/{{$imagem->imagem}}" rel="galeria-{{$clipping->id}}" class="abre-galeria-clippings" style="display:none;"></a>
							@endif
						@endforeach
					@endif
				</li>
			@endforeach
			</ul>
		</div>
	@else
		<h1 class="sem-resultado padded">{{ Lang::get('geral.nenhum_clipping')}}</h1>
	@endif

@stop