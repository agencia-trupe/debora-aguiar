@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Parceiros <a href="{{ URL::route('painel.parceiros.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Parceiro</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="parceiros">

          		<thead>
            		<tr>
                    <th style="width:40px;">ordenar</th>
              			<th>Título</th>
              			<th>Imagem</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($parceiros as $parceiro)

                	<tr class="tr-row" id="row_{{$parceiro->id}}">
                      <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
                  		<td>{{ $parceiro->titulo_pt }}</td>
                  		<td><img src="assets/images/parceiros/{{ $parceiro->imagem }}"></td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.parceiros.edit', $parceiro->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.parceiros.destroy', $parceiro->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.parceiros.destroy', $parceiro->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop