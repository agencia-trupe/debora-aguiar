@section('conteudo')

	<div class="detalhe-projeto">
		
		<!-- Título -->
		<h1>{{ $projeto->{Concat::sufixo('titulo')} }}</h1>
		
		<!-- Imagens médias e texto de Fornecedores -->
		<div id="ampliadas">

			<div class="viewport-ampliadas">

				@if($imagens)
					@foreach($imagens as $h => $imagem)
						<a href="assets/images/portfolio/{{$imagem->imagem}}" rel="ampliadas" title="{{ $projeto->{Concat::sufixo('titulo')} }} - {{Lang::get('Foto')}} {{$h+1}}" style="background-image:url('assets/images/portfolio/medias/{{$imagem->imagem}}')">
							<img src="assets/images/portfolio/medias/{{$imagem->imagem}}" alt="{{ $projeto->{Concat::sufixo('titulo')} }}">
						</a>
					@endforeach					
				@endif

				@if($projeto->projetos_categorias_id == '41' && $projeto->fornecedores != '')
					<div class="fornecedores-card">
						{{$projeto->fornecedores}}
					</div>
				@endif

			</div>

		</div>
		
		<!-- Imagens pequenas e navegação -->
		<div id="thumbs">

			<a href="#" title="{{Lang::get('Thumbs Anteriores')}}" class="prev-btn">{{Lang::get('Thumbs Anteriores')}}</a>

			<div class="viewport-thumbs">
				@if($imagens)

					<div class="move-handle">
						@foreach($imagens as $k => $imagem)

							<a href="" title="{{ $projeto->{Concat::sufixo('titulo')} }} - {{Lang::get('Foto')}} {{$k+1}}">
								<img src="assets/images/portfolio/thumbs/{{$imagem->imagem}}" alt="{{ $projeto->{Concat::sufixo('titulo')} }}">
							</a>

						@endforeach
					</div>

				@endif
			</div>

			<a href="#" title="{{Lang::get('Próximas Thumbs')}}" class="next-btn">{{Lang::get('Próximas Thumbs')}}</a>

		</div>
		
		
		<!-- Navegação Fornecedores -->
		@if($projeto->projetos_categorias_id == '41' && $projeto->fornecedores != '')
			<a href="#" title="{{Lang::get('FORNECEDORES')}}" class="fornecedores-open">{{Lang::get('FORNECEDORES')}}</a>
		@endif


		<!-- Navegação dos Projetos -->
		<div id="navegacao">
		
			<div class="voltar">
				<a href="portfolio/{{ $objCategoria->{Concat::sufixo('slug')} }}" title="{{Lang::get('HOME PROJETOS')}}">&laquo; {{Lang::get('HOME PROJETOS')}}</a>
			</div>

			<div class="navegar">
				@if($nav['anterior']['existe'] == true)
					<a href="{{$nav['anterior']['url']}}" title="{{Lang::get('PROJETO ANTERIOR')}}" class="prev">&laquo; {{Lang::get('PROJETO ANTERIOR')}}</a>
				@endif

				@if($nav['anterior']['existe'] == true && $nav['proxima']['existe'] == true)
					<span class="separador">|</span>
				@endif
				
				@if($nav['proxima']['existe'] == true)
					<a href="{{$nav['proxima']['url']}}" title="{{Lang::get('PRÓXIMO PROJETO')}}" class="next">{{Lang::get('PRÓXIMO PROJETO')}} &raquo;</a>
				@endif
			</div>

		</div>

	</div>

@stop