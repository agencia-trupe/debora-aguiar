@section('conteudo')

	<div class="pure-g-r contato-grid">
		
		<div class="pure-u-11-24">
			<div class="contato">
				<div class="endereco">
					@if(!is_null($contato))
						{{ $contato->{Concat::sufixo('texto')} }}
					@endif					
				</div>
				<div class="form">

					<h2>
						{{ Lang::get('contato.titulo') }}
					</h2>

					@if(Session::has('envio'))
						<div class="envio sucesso">
							{{ Lang::get('contato.mensagem_enviada') }}
							<br>
							{{ Lang::get('contato.retornaremos') }}
						</div>
					@endif
					<form action="contato" method="post">
						<input type="text" name="nome" placeholder="{{ Lang::get('contato.nome') }}" required data-required-text="{{ Lang::get('contato.nome_required') }}">
						<input type="email" name="email" placeholder="{{ Lang::get('contato.email') }}" required data-required-text="{{ Lang::get('contato.email_required') }}">
						<input type="text" name="telefone" placeholder="{{ Lang::get('contato.telefone') }}">
						<textarea name="mensagem" placeholder="{{ Lang::get('contato.mensagem') }}" required data-required-text="{{ Lang::get('contato.mensagem_required') }}"></textarea>
						<input type="submit" value="{{ Lang::get('contato.enviar') }}">
					</form>
				</div>
			</div>
		</div>

		<div class="pure-u-13-24">
			<div class="mapa">
				<h2>
					{{ Lang::get('contato.como_chegar') }}
					<a href="https://maps.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Bento+de+Andrade,+660,+Jardim+Paulista,+S%C3%A3o+Paulo&amp;aq=0&amp;oq=Rua+Bento+de+Andrade,+660&amp;sll=-23.682412,-46.595299&amp;sspn=0.918071,1.584778&amp;ie=UTF8&amp;hq=&amp;hnear=Rua+Bento+de+Andrade,+660+-+Jardim+Paulista,+S%C3%A3o+Paulo,+04503-001&amp;t=m&amp;z=14&amp;ll=-23.584528,-46.668993">{{ Lang::get('contato.ver_mapa') }}</a>
				</h2>
				<div class="mapa_iframe">
					<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Bento+de+Andrade,+660,+Jardim+Paulista,+S%C3%A3o+Paulo&amp;aq=0&amp;oq=Rua+Bento+de+Andrade,+660&amp;sll=-23.682412,-46.595299&amp;sspn=0.918071,1.584778&amp;ie=UTF8&amp;hq=&amp;hnear=Rua+Bento+de+Andrade,+660+-+Jardim+Paulista,+S%C3%A3o+Paulo,+04503-001&amp;t=m&amp;z=14&amp;ll=-23.584528,-46.668993&amp;output=embed"></iframe>
				</div>
			</div>
		</div>

	</div>

@stop