@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Banner
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.banners.update', $banner->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-banner') ) }}

			<label>Imagem<br>
			@if($banner->imagem)
				<img src="assets/images/home/thumbs/{{ $banner->imagem }}"><br>
				<a href="assets/images/home/{{ $banner->imagem }}" title="ver tamanho original" target="_blank">ver tamanho original</a><br>
			@endif
			<input type="file" name="imagem"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop