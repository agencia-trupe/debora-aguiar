@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Clipping Digital
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.clippingDigital.store', 'method' => 'post', 'id' => 'form-create-clip-dig') ) }}

			<label>Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<input type="text" name="titulo_pt" required required-message="Informe o título do parceiro!" class="input-xxlarge">
			</label>

			<label>Título - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
				<input type="text" name="titulo_en" required required-message="Informe o título do parceiro!" class="input-xxlarge">
			</label>
			
			<label>
				Data<br>
				<input type="text" name="data" class="datepicker" required required-message="Informe a data do Clipping!">
			</label>

			<label>Descrição - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<textarea name="descricao_pt" class="completo pequeno"></textarea>
			</label>

			<label>Descrição - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
				<textarea name="descricao_en" class="completo pequeno"></textarea>
			</label>

			<label>Link<br>
				<input type="text" name="link" class="input-xxlarge" required required-message="Informe o link de destino do Clipping!">
			</label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop