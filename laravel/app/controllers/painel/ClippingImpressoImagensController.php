<?php

namespace Painel;

use View, Input, Str, Session, Redirect, File, Imagine, ClippingImpresso, ClippingImpressoImagem;

class ClippingImpressoImagensController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clipping_impresso_id = Input::get('clipping_impresso_id');
		$this->layout->content = View::make('painel.clippingImagens.index')->with('imagens', ClippingImpressoImagem::where('clipping_impresso_id', '=', $clipping_impresso_id)->orderBy('ordem')->get())
																   		   ->with('clippingImpresso', ClippingImpresso::find($clipping_impresso_id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($clipping_impresso_id)
	{
		$this->layout->content = View::make('painel.clippingImagens.form')->with('clippingImpresso', ClippingImpresso::find($clipping_impresso_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ClippingImpressoImagem;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/clippings/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/clippings/', $filename);
			$object->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeThumb = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/clippings/'.$filename)
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/clippings/thumbs/'.$filename, array('quality' => 100));
		}
		$object->clipping_impresso_id = Input::get('clipping_impresso_id');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem inserida com sucesso.');

		return Redirect::route('painel.clippingImpressoImagens.index', array('clipping_impresso_id' => $object->clipping_impresso_id));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($clipping_impresso_id, $id)
	{
		$this->layout->content = View::make('painel.clippingImagens.edit')->with('imagem', ClippingImpressoImagem::find($id))
																  		  ->with('clippingImpresso', ClippingImpresso::find($clipping_impresso_id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ClippingImpressoImagem::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/clippings/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/clippings/', $filename);
			$object->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeThumb = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/clippings/'.$filename)
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/clippings/thumbs/'.$filename, array('quality' => 100));
		}
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem alterado com sucesso.');

		return Redirect::route('painel.clippingImpressoImagens.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ClippingImpressoImagem::find($id);
		$id_clipping = $object->clipping->id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removido com sucesso.');

		return Redirect::route('painel.clippingImpressoImagens.index', array('clipping_impresso_id' => $id_clipping));
	}

}