@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Clipping Impresso
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.clippingImpresso.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-clip-imp') ) }}			

				<label>
					Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
					<input type="text" name="titulo_pt" required required-message="Informe o Título da publicação!">	
				</label>

				<label>
					Título - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
					<input type="text" name="titulo_en" required required-message="Informe o Título da publicação!">	
				</label>

				<label>
					Data de Publicação<br>
					<input type="text" name="data" class="datepicker" required required-message="Informe a Data da publicação!">
				</label>

				<label>Imagem de Capa<br>
				<input type="file" name="imagem" required></label>

				<div class="form-actions">
		        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
		        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
		      	</div>
		      	
			{{ Form::close() }}

		</div>
	</div>

@stop