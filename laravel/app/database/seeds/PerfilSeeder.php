<?php

class PerfilSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'perfil_pt' => 'Texto Perfil (pt-br)',
				'perfil_en' => 'Texto Perfil (en)',
				'metodologia_pt' => 'Texto Metodologia (pt-br)',
				'metodologia_en' => 'Texto Metodologia (en)',
				'imagem' => 'imagem',
            ]
        ];

        DB::table('perfil')->delete();
        DB::table('perfil')->insert($data);
    }

}