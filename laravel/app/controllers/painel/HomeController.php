<?php

namespace Painel;

use View;

class HomeController extends BaseAdminController {

    protected $layout = 'painel.template';

	public function index()
	{
		$this->layout->content = View::make('painel.home');
	}

	public function login()
	{
		return View::make('painel.login');
	}

}