@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Parceiro
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.parceiros.store', 'files' => true,'method' => 'post', 'id' => 'form-create-parceiro') ) }}

			<label>Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
			<input type="text" name="titulo_pt" required required-message="Informe o título do parceiro!" class="input-xxlarge"></label>

			<label>Título - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
			<input type="text" name="titulo_en" required required-message="Informe o título do parceiro!" class="input-xxlarge"></label>

			<label>Imagem<br>
			<input type="file" name="imagem" required required-message="Selecione a imagem da marca do parceiro!"></label>

			<label>Link<br>
			<input type="text" name="link">
			</label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop