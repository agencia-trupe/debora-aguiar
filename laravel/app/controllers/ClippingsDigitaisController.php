<?php

use \ClippingDigital;

class ClippingsDigitaisController extends BaseController {

	protected $layout = 'frontend.template';

	public function index()
	{
		$this->layout->content = View::make('frontend.clippings.digitais')->with('clippings', ClippingDigital::orderBy('ordem')->get());
	}

}