<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <link rel="shortcut icon" href="">
    <meta name="keywords" content="" />

	<title>{{ Lang::get('geral.titulo_site') }}</title>
	<meta name="description" content="{{ Lang::get('geral.description') }}">
	<meta property="og:description" content="{{ Lang::get('geral.description') }}"/>
	<meta property="og:title" content="{{ Lang::get('geral.titulo_site') }}"/>
	<meta property="og:image" content="{{ asset('assets/images/layout/fb.jpg'); }}"/>
    
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
  	<meta property="fb:admins" content="100002297057504"/>
  	<meta property="fb:app_id" content="262328160587194"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'reset',
		'pure/pure-css',
		'fontface/stylesheet'		
	))?>

	@if(isset($load_css))
		<?=Assets::CSS($load_css)?>	
	@endif

	@if(Route::currentRouteName() == 'home')
		<?=Assets::CSS('home')?>	
	@else
		<?=Assets::CSS('internas')?>	
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1', 'libs/modernizr-2.0.6.min', 'libs/less-1.3.0.min'))?>
	@else
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1', 'libs/modernizr-2.0.6.min'))?>
	@endif

</head>
<body>
	
	<!-- Facebook SDK -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=262328160587194";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<!-- Twitter SDK -->
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	
	<aside>
		<div class="insidepad">

			<a href="{{ URL::route('home') }}" title="{{ Lang::get('menu.home_titulo') }}" id="link-home">
				<img src="assets/images/layout/marca-deboraaguiar.png" alt="{{ Lang::get('geral.titulo_site') }}">
			</a>

			<nav>
				<ul>
					<li>
						<a href="{{ URL::route('home') }}" title="{{ Lang::get('menu.home_titulo') }}" @if(Route::currentRouteName() == 'home') class='ativo' @endif>{{ Lang::get('menu.home') }}</a>
					</li>
					<li>
						<a href="{{ URL::route('perfil') }}" title="{{ Lang::get('menu.perfil_titulo') }}" @if(Route::currentRouteName() == 'perfil') class='ativo' @endif>{{ Lang::get('menu.perfil') }}</a>
					</li>
					<li>
						<a href="{{ URL::route('portfolio') }}" class="abre-sub @if(str_is('portfolio*', Route::currentRouteName())) ativo @endif" title="{{ Lang::get('menu.portfolio_titulo') }}">{{ Lang::get('menu.portfolio') }}</a>
						<ul class="submenu @if(!str_is('portfolio*', Route::currentRouteName())) escondido @endif">
							@if(isset($submenu_portfolio) && $submenu_portfolio)
								@foreach($submenu_portfolio as $item)
									<li>
										<a href="{{ URL::to('portfolio/'.$item->{Concat::sufixo('slug')}) }}" @if(isset($categoriaSelecionada) && $categoriaSelecionada == $item->{Concat::sufixo('slug')}) class="ativo" @endif title="{{ $item->{Concat::sufixo('titulo')} }}">{{ $item->{Concat::sufixo('titulo')} }}</a>
									</li>
								@endforeach
							@endif							
						</ul>
					</li>
					<li>
						<a href="{{ URL::route('clientes') }}" title="{{ Lang::get('menu.clientes_titulo') }}" @if(Route::currentRouteName() == 'clientes') class='ativo' @endif>{{ Lang::get('menu.clientes') }}</a>
					</li>
					<li>
						<a href="{{ URL::route('parceiros') }}" title="{{ Lang::get('menu.parceiros_titulo') }}" @if(Route::currentRouteName() == 'parceiros') class='ativo' @endif>{{ Lang::get('menu.parceiros') }}</a>
					</li>
					<li>
						<a href="clippings/impressos" class="abre-sub" title="{{ Lang::get('menu.clipping_titulo') }}" @if(Route::currentRouteName() == 'clipping') class='ativo' @endif>{{ Lang::get('menu.clipping') }}</a>
						<ul class="submenu @if(!str_is('clipping*', Route::currentRouteName())) escondido @endif">
							<li>
								<a href="clippings/impressos" title="{{ Lang::get('menu.clipping_impresso_titulo') }}">{{ Lang::get('menu.clipping_impresso') }}</a>
							</li>
							<li>
								<a href="clippings/digitais" title="{{ Lang::get('menu.clipping_digital_titulo') }}">{{ Lang::get('menu.clipping_digital') }}</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="{{ URL::route('contato') }}" title="{{ Lang::get('menu.contato_titulo') }}" @if(Route::currentRouteName() == 'contato') class='ativo' @endif>{{ Lang::get('menu.contato') }}</a>
					</li>
				</ul>
			</nav>

			<div id="menu-bottom">

				<div id="barra">
					[<a href="" target="" title="{{ Lang::get('menu.area_restrita_titulo') }}"><img src="assets/images/layout/cadeado.png" alt="{{ Lang::get('menu.area_restrita_titulo') }}">{{ Lang::get('menu.area_restrita') }}</a>] &bull;
					@if(Config::get('app.locale') == 'pt')
						[<a href="trocarIdioma/en" title="{{ Lang::get('menu.versao_ingles_titulo') }}">{{ Lang::get('menu.versao_ingles') }}</a>]
					@else
						[<a href="trocarIdioma/pt" title="{{ Lang::get('menu.versao_portugues_titulo') }}">{{ Lang::get('menu.versao_portugues') }}</a>]
					@endif
				</div>

				<div id="assinatura">
					&copy; {{ Date('Y')}} {{ Lang::get('menu.assinatura') }}
					<br>
					<a href="http://www.trupe.net" target="_blank" title="{{ Lang::get('menu.criacao_sites_titulo') }}">{{ Lang::get('menu.criacao_sites') }}</a> : <a href="http://www.trupe.net" target="_blank" title="{{ Lang::get('menu.criacao_sites_titulo') }}">{{ Lang::get('menu.trupe') }}</a>
				</div>

			</div>
			
		</div>
	</aside>

	<section>
		
		<!-- Conteúdo Principal -->
		<h1 class="sem-resultado">{{ Lang::get('404') }}</h1>

	</section>
	
	@if(isset($load_js))
		<?=Assets::JS($load_js)?>	
	@endif	

	<?=Assets::JS(array(
		'scripts/main',
	))?>
	
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
		_gaq.push(['_trackPageview']);
		(function() {
		   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		   ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
		})();
	</script>
	
</body>
</html>