@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Clippings Impressos <a href="{{ URL::route('painel.clippingImpresso.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Clipping</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed">

          		<thead>
            		<tr>
                        <th>Título</th>
                        <th>Data</th>
                        <th>Capa</th>
                        <th>Páginas do Clipping</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($clippings as $registro)

                	<tr class="tr-row" id="row_{{$registro->id}}">
                        <td>
                            PT: {{ $registro->titulo_pt }}
                            <br><br>                        
                            EN: {{ $registro->titulo_en }}
                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($registro->data)) }}
                        </td>
                        <td><img src="assets/images/clippings/thumbs/{{ $registro->capa }}" style="width:140px;"></td>
                        <td>
                            <a href="{{ URL::route('painel.clippingImpressoImagens.index', array('clipping_impresso_id' => $registro->id)) }}" class="btn btn-info">imagens</a>
                        </td>
                        <td class="crud-actions">
                  		    <a href="{{ URL::route('painel.clippingImpresso.edit', $registro->id ) }}" class="btn btn-primary">editar</a>

                            {{ Form::open(array('route' => array('painel.clippingImpresso.destroy', $registro->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.clippingImpresso.destroy', $registro->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
                            {{ Form::close() }}
                        </td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop