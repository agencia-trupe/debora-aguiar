<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClippingDigitalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clipping_digital', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo_pt')->nullable();
			$table->string('titulo_en')->nullable();
			$table->date('data');
			$table->text('descricao_pt')->nullable();
			$table->text('descricao_en')->nullable();
			$table->string('link');
			$table->integer('ordem')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clipping_digital');
	}

}