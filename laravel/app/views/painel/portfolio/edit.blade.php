@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Projeto
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.portfolio.update', $portfolio->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-portfolio') ) }}

			<label>Categoria<br>
			<select name="projetos_categorias_id" required required-message="A Categoria é obrigatória!">
				<option value="">Selecione</option>
				@if($categorias)
					@foreach($categorias as $categoria)
						<option value="{{$categoria->id}}" @if($portfolio->projetos_categorias_id == $categoria->id) selected @endif >{{$categoria->titulo_pt}}</option>
					@endforeach
				@endif
			</select>
			</label>

			<label>Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
			<input type="text" name="titulo_pt" required required-message="O título é obrigatório!" class="input-xxlarge" value="{{$portfolio->titulo_pt}}"></label>

			<label>Título - Versão em Inglês <img src="blank.gif" class="flag flag-us">  	<br>
			<input type="text" name="titulo_en" required required-message="O título é obrigatório!" class="input-xxlarge" value="{{$portfolio->titulo_en}}"></label>
			
			<label>Imagem<br>
				@if($portfolio->capa)
					<img src="assets/images/portfolio/thumbs/{{ $portfolio->capa }}"><br>				
				@endif
				<input type="file" name="imagem">
			</label>
			
			@if($portfolio->projetos_categorias_id != '41')
				<label id="forn-txt" style="display:none">Fornecedores<br>
			@else
				<label id="forn-txt">Fornecedores<br>
			@endif
			<textarea name="fornecedores" class="completo medio">{{$portfolio->fornecedores}}</textarea></label>
			
			@if($portfolio->projetos_categorias_id != '42')
				<label id="swf-txt" style="display:none">Link para a apresentação (Deve iniciar com http://)<br>
			@else
				<label id="swf-txt">Link para a apresentação (Deve iniciar com http://)<br>
			@endif			
			<input type="text" name="swf" class="input-xxlarge" value="{{$portfolio->link}}"></label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop