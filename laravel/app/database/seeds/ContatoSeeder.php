<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto_pt' => 'Texto Contato (pt-br)',
				'texto_en' => 'Texto Contato (en)',
            ]
        ];

        DB::table('contato')->delete();
        DB::table('contato')->insert($data);
    }

}