<?php

namespace Painel;

use View, Input, Str, Session, Redirect, ClippingDigital;

class ClippingDigitalController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('painel.clippingDigital.index')->with('clipping', ClippingDigital::orderBy('ordem')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.clippingDigital.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ClippingDigital;
		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->data = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",Input::get('data'));
		$object->descricao_pt = Input::get('descricao_pt');
		$object->descricao_en = Input::get('descricao_en');
		$object->link = Input::get('link');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Clipping criado com sucesso.');

		return Redirect::route('painel.clippingDigital.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.clippingDigital.edit')->with('clipping', ClippingDigital::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ClippingDigital::find($id);
		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->data = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",Input::get('data'));
		$object->descricao_pt = Input::get('descricao_pt');
		$object->descricao_en = Input::get('descricao_en');
		$object->link = Input::get('link');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Clipping alterado com sucesso.');

		return Redirect::route('painel.clippingDigital.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ClippingDigital::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Clipping removido com sucesso.');

		return Redirect::route('painel.clippingDigital.index');
	}

}