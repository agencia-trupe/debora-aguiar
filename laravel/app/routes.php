<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('perfil', array('as' => 'perfil', 'uses' => 'PerfilController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));

Route::get('trocarIdioma/{lang?}', function($lang = ''){
	
	if($lang == 'pt')
		Session::put('idioma', 'pt');
	elseif($lang == 'en')
		Session::put('idioma', 'en');
	
	return Redirect::back();
});

Route::get('portfolio/{categoria?}/{projeto?}', array('as' => 'portfolio', 'uses' => 'PortfolioController@index'));
Route::get('clientes', array('as' => 'clientes', 'uses' => 'ClientesController@index'));
Route::get('parceiros', array('as' => 'parceiros', 'uses' => 'ParceirosController@index'));
Route::get('clippings/impressos/', array('as' => 'clippings.impressos', 'uses' => 'ClippingsImpressosController@index'));
Route::get('clippings/noshadow/{id}', array('as' => 'clippings.noshadow', 'uses' => 'ClippingsImpressosController@noShadow'));
Route::get('clippings/digitais', array('as' => 'clippings.digitais', 'uses' => 'ClippingsDigitaisController@index'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('banners', 'Painel\BannersController');
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('perfil', 'Painel\PerfilController');
    Route::resource('portfolio', 'Painel\PortfolioController');
    Route::resource('imagens', 'Painel\PortfolioImagensController');
    Route::resource('clientes', 'Painel\ClientesController');
    Route::resource('parceiros', 'Painel\ParceirosController');
    Route::resource('clippingImpresso', 'Painel\ClippingImpressoController');
    Route::resource('clippingImpressoImagens', 'Painel\ClippingImpressoImagensController');
    Route::resource('clippingDigital', 'Painel\ClippingDigitalController');
    Route::resource('contato', 'Painel\ContatoController');
});


Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);
	if(Auth::attempt($authvars)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));