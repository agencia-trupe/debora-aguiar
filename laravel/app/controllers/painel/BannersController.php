<?php

// Namespace
namespace Painel;

// Core
use View, Input, Str, Session, Redirect, File;

// Models
use Banner;

// Libs
use Imagine;

class BannersController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('painel.banners.index')->with('banners', Banner::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.banners.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$banner = new Banner;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/home/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));

			$imagem->move('assets/images/home/', $filename);
			$banner->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/home/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/home/thumbs/'.$filename, array('quality' => 100));
		}
		$banner->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Banner criado com sucesso.');

		return Redirect::route('painel.banners.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.banners.edit')->with('banner', Banner::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$banner = Banner::find($id);

		if(Input::hasFile('imagem')){
			$imagem = Input::file('imagem');
			
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/home/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));

			$imagem->move('assets/images/home/', $filename);
			$banner->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/home/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/home/thumbs/'.$filename, array('quality' => 100));
		}
		$banner->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Banner alterado com sucesso.');

		return Redirect::route('painel.banners.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$banner = Banner::find($id);
		$banner->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Banner removido com sucesso.');

		return Redirect::route('painel.banners.index');
	}

}