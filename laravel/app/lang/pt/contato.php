<?php

return array(

	'titulo' => 'CONTATO',

	'nome' => 'nome',
	'nome_required' => 'Informe seu nome!',

	'email' => 'e-mail',
	'email_required' => 'Informe seu e-mail!',

	'telefone' => 'telefone',

	'mensagem' => 'mensagem',
	'mensagem_required' => 'Informe sua mensagem!',

	'enviar' => 'ENVIAR',
	
	'como_chegar' => 'COMO CHEGAR',
	'ver_mapa' => 'ver mapa ampliado &raquo;',

	'mensagem_enviada' => 'Sua mensagem foi enviada com sucesso!',
	'retornaremos' => 'Entraremos em contato assim que possível.',
);