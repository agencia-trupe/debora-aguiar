@section('conteudo')

	<div id="slides">
		@if(sizeof($banners) > 0)
			@foreach($banners as $banner)
				<div class="slide" style="background-image:url('assets/images/home/{{$banner->imagem}}')"></div>	
			@endforeach
		@endif
	</div>

	<div id="slide-mobile">
		<img src="assets/images/home/{{$banners[rand(0, sizeof($banners) - 1)]->imagem}}">
	</div>

@stop