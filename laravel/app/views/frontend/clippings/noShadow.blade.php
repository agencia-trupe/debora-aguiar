@section('conteudo')

	<a href="javascript:history.back()" title="Voltar" class="btn-voltar">&laquo; voltar</a>

	@if($clipping)
		<img src="assets/images/clippings/{{$clipping->capa}}" alt="{{$clipping->titulo}}" style="max-width:100%; margin-bottom:4px; ">
		@if($clipping->imagens)
		    @foreach($clipping->imagens as $v)
		    	<img src="assets/images/clippings/{{$v->imagem}}" alt="{{$clipping->titulo}}" style="max-width:100%; margin-bottom:4px; ">
		    @endforeach
		@endif

	@endif

	<a href="javascript:history.back()" title="Voltar" class="btn-voltar">&laquo; voltar</a>

@stop