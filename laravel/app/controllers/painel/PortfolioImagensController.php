<?php

namespace Painel;

use View, Input, Str, Session, Redirect, File, Imagine, Portfolio, PortfolioImagens;

class PortfolioImagensController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$portfolio_id = Input::get('portfolio_id');
		$this->layout->content = View::make('painel.imagens.index')->with('imagens', PortfolioImagens::where('projetos_id', '=', $portfolio_id)->orderBy('ordem')->get())
																   ->with('portfolio', Portfolio::find($portfolio_id));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($portfolio_id)
	{
		$this->layout->content = View::make('painel.imagens.form')->with('portfolio', Portfolio::find($portfolio_id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new PortfolioImagens;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/portfolio/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/portfolio/', $filename);
			$object->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(700,700);
			$sizeThumb = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/portfolio/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/portfolio/medias/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/portfolio/thumbs/'.$filename, array('quality' => 100));
		}
		$object->projetos_id = Input::get('projetos_id');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem inserida com sucesso.');

		return Redirect::route('painel.imagens.index', array('portfolio_id' => $object->projetos_id));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($portfolio_id, $id)
	{
		$this->layout->content = View::make('painel.imagens.edit')->with('imagem', PortfolioImagens::find($id))
																  ->with('portfolio', Portfolio::find($portfolio_id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = PortfolioImagens::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/portfolio/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$imagem->move('assets/images/portfolio/', $filename);
			$object->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(700,700);
			$sizeThumb = new Imagine\Image\Box(200,200);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/portfolio/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/portfolio/medias/'.$filename, array('quality' => 100))
					->thumbnail($sizeThumb, $mode)
					->save('assets/images/portfolio/thumbs/'.$filename, array('quality' => 100));
		}
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem alterado com sucesso.');

		return Redirect::route('painel.imagens.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = PortfolioImagens::find($id);
		$id_projeto = $object->portfolio->id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removido com sucesso.');

		return Redirect::route('painel.imagens.index', array('portfolio_id' => $id_projeto));
	}

}