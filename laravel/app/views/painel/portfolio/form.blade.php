@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Projeto
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.portfolio.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-portfolio') ) }}

				<label>Categoria<br>
				<select name="projetos_categorias_id" required required-message="A Categoria é obrigatória!">
					<option value="">Selecione</option>
					@if($categorias)
						@foreach($categorias as $categoria)
							<option value="{{$categoria->id}}">{{$categoria->titulo_pt}}</option>
						@endforeach
					@endif
				</select>
				</label>

				<label>Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<input type="text" name="titulo_pt" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

				<label>Título - Versão em Inglês <img src="blank.gif" class="flag flag-us">  	<br>
				<input type="text" name="titulo_en" required required-message="O título é obrigatório!" class="input-xxlarge"></label>
				
				<label>Imagem<br>
				<input type="file" name="imagem" required required-message="A imagem é obrigatória!"></label>

				<label id="forn-txt" style="display:none">Fornecedores<br>
				<textarea name="fornecedores" class="completo medio"></textarea></label>

				<label id="swf-txt" style="display:none">Link para a apresentação (Deve iniciar com http://)<br>
				<input type="text" name="swf" class="input-xxlarge"></label>

				<div class="form-actions">
		        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
		        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
		      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop