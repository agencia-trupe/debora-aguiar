<?php

return array(

	'titulo_site' => 'Debora Aguiar Arquitetos',
	'titulo_reduzido' => 'Debora Aguiar',

	'404' => 'Página não encontrada',

	'nenhum_projeto' => 'Nenhum projeto encontrado para a Categoria',
	'nenhum_clipping' => 'Nenhum clipping encontrado',
	'nenhum_cliente' => 'Nenhum cliente encontrado',
	'nenhum_parceiro' => 'Nenhum parceiro encontrado',

	'Foto' => 'Foto',
	'Thumbs Anteriores' => 'Thumbs Anteriores',
	'Próximas Thumbs' => 'Próximas Thumbs',
	
	'HOME PROJETOS' => 'HOME PROJETOS',
	'PROJETO ANTERIOR' => 'PROJETO ANTERIOR',
	'PRÓXIMO PROJETO' => 'PRÓXIMO PROJETO',
	'FORNECEDORES' => 'FORNECEDORES'
);