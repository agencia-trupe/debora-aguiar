@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Usuário
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.usuarios.store', 'method' => 'post', 'id' => 'form-create-usuario') ) }}

			<label>Usuário<br>
			<input type="text" name="username" required autofocus class="input-xxlarge"></label>

			<label>E-mail<br>
			<input type="email" name="email" required class="input-xxlarge"></label>

			<label>Senha<br>
			<input type="password" name="password" id="senha" required></label>

			<label>Confirmar Senha<br>
			<input type="password" id="conf-senha" required></label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop