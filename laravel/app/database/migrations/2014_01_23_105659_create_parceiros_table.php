<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParceirosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parceiros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo_pt')->nullable();
			$table->string('titulo_en')->nullable();
			$table->string('link')->nullable();
			$table->string('imagem');
			$table->integer('ordem')->default('0');			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parceiros');
	}

}