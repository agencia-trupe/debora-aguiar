<?php

class UsersSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'email' => 'contato@trupe.net',
            	'username' => 'trupe',
            	'password' => Hash::make('senhatrupe')
            ]
        ];

        DB::table('users')->delete();
        DB::table('users')->insert($data);
    }

}