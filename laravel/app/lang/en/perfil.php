<?php

return array(
	'perfil_paragrafo_1' => 'Counting with over 200 consolidated clients among top construction companies of the Country and based on a long-term relationship, the office is constantly pursuing challenging opportunities to transform creative and innovative ideas into well-resolved projects assuring beauty and sophistication together with comfort, security, dynamism and assertiveness.',
	'perfil_paragrafo_2' => 'Projects are developed with total responsibility and availability in regards to client services and management, whereas production is structured by a planning strategy based on clients needs and working methodologies with strict observation to turnaround timings, budget limits and deadlines. Also a calculated financial control is mandatory to guarantee the best cost-benefit of the final product, as well as the state-of-the-art quality of the services offered.',
	'perfil_paragrafo_3' => '',
	'metodologia_titulo' => 'METODOLOGY',
	'metodologia_fidelidade_titulo' => 'FIDELITY',
	'metodologia_fidelidade_texto' => 'We offer custom-made services establishing a transparent relationship with the client from beginning to completion of the project.',
	'metodologia_viabilidade_titulo' => 'VIABILITY',
	'metodologia_viabilidade_texto' => 'We pursue the balance between client needs, wishes and reality, according to budget planning through well-calculated estimates.',
	'metodologia_planejamento_titulo' => 'PLANNING & COORDINATION',
	'metodologia_planejamento_texto' => 'We define all steps of the process aligning goals, deadlines and necessities between clients and suppliers.',
	'metodologia_realizacao_titulo' => 'REALIZATION',
	'metodologia_realizacao_texto' => 'We reach our goals by offering a successful product assuring full satisfaction to the client together with market recognition.',
);