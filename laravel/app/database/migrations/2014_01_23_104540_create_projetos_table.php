<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projetos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('projetos_categorias_id');
			$table->string('titulo_pt')->nullable();
			$table->string('titulo_en')->nullable();
			$table->string('slug_pt');
			$table->string('slug_en');
			$table->string('capa');
			$table->text('fornecedores')->nullable();
			$table->integer('ordem')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projetos');
	}

}