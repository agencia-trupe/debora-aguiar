@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Perfil
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.perfil.update', $perfil->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-perfil') ) }}

				<label>Perfil - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<textarea name="perfil_pt" class="completo grande">{{ $perfil->perfil_pt }}</textarea></label>

				<label>Perfil - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
				<textarea name="perfil_en" class="completo grande">{{ $perfil->perfil_en }}</textarea></label>

				<label>Metodologia - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<textarea name="metodologia_pt" class="completo grande">{{ $perfil->metodologia_pt }}</textarea></label>

				<label>Metodologia - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
				<textarea name="metodologia_en" class="completo grande">{{ $perfil->metodologia_en }}</textarea></label>

				<label>Imagem<br>
				@if($perfil->imagem)
					<img src="assets/images/perfil/{{ $perfil->imagem }}" style="width:200px;"><br>				
				@endif
				<input type="file" name="imagem"></label>

				<div class="form-actions">
		        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
		        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
		      	</div>
			{{ Form::close() }}

		</div>
	</div>

@stop