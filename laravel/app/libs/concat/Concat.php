<?php
/**
*	Classe para inserir Prefixo/Sufixo de linguagem em strings
*	@author Bruno Monteiro
*	@since 29/01/2014
*/
class Concat
{

	public static function prefixo($string = ''){
		return Session::has('idioma') ? Session::get('idioma').'_'.$string : 'pt_'.$string;
	}

	public static function sufixo($string = ''){
		return Session::has('idioma') ? $string.'_'.Session::get('idioma') : $string.'_pt';	
	}

}