<?php

use \Perfil, \Concat;

class PerfilController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout = 'frontend.template';

	public function index()
	{
		$this->layout->content = View::make('frontend.perfil')->with('perfil', Perfil::first());
	}

}