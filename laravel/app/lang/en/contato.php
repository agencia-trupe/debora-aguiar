<?php

return array(

	'titulo' => 'CONTACT',

	'nome' => 'name',
	'nome_required' => 'Please inform your name!',

	'email' => 'e-mail',
	'email_required' => 'Please inform your e-mail!',

	'telefone' => 'phone',

	'mensagem' => 'message',
	'mensagem_required' => 'Please inform a message!',

	'enviar' => 'SEND',

	'como_chegar' => 'LOCATION',
	'ver_mapa' => 'see full-size map &raquo;',
	
	'mensagem_enviada' => 'Your message was sent successfully!',
	'retornaremos' => 'We\'ll get in touch as soon as possible.',
);