@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Clippings Digitais <a href="{{ URL::route('painel.clippingDigital.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Clipping</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-bordered table-condensed table-striped table-sortable" data-tabela="clipping_digital">

          		<thead>
            		<tr>
                    <th>ordernar</th>
                    <th>Título</th>
                    <th>Data</th>
                    <th>Descrição</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($clipping as $registro)

                	<tr class="tr-row" id="row_{{$registro->id}}">
                        <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
                        <td>
                            PT: {{ $registro->titulo_pt }}
                            <br><br>
                            EN: {{ $registro->titulo_en }}
                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($registro->data)) }}
                        </td>
                        <td>
                            PT: {{ Str::words($registro->descricao_pt, 10) }}
                            <br><br>
                            EN: {{ Str::words($registro->descricao_en, 10) }}
                        </td>
                        <td class="crud-actions">
                  		    <a href="{{ URL::route('painel.clippingDigital.edit', $registro->id ) }}" class="btn btn-primary">editar</a>

                            {{ Form::open(array('route' => array('painel.clippingDigital.destroy', $registro->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.clippingDigital.destroy', $registro->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
                            {{ Form::close() }}
                        </td>
                	</tr>                  

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop