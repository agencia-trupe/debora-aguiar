<?php

namespace Painel;

// Core
use View, Input, Str, Session, Redirect;

// Models
use Perfil;

// Libs
use Imagine;

class PerfilController extends BaseAdminController {

	protected $layout = 'painel.template';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('painel.perfil.index')->with('perfil', Perfil::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('painel.perfil.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Perfil;

		/***********************************/
		/*		Upload de Imagem 	   	   */
		/*   só manter caso exista imagem  */
		/***********************************/
		$imagem = Input::file('imagem');
		$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();
		$imagem->move('assets/images/perfil', $filename);
		$object->imagem = $filename;
		/***********************************/

		$object->perfil_pt = Input::get('perfil_pt');
		$object->perfil_en = Input::get('perfil_en');
		$object->metodologia_pt = Input::get('metodologia_pt');
		$object->metodologia_en = Input::get('metodologia_en');
		//$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto criado com sucesso.');

		return Redirect::route('painel.perfil.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('painel.perfil.edit')->with('perfil', Perfil::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Perfil::find($id);

		/***********************************/
		/*		Upload de Imagem 	   	   */
		/*   só manter caso exista imagem  */
		/***********************************/
		if(Input::hasFile('imagem')){
			$imagem = Input::file('imagem');
			$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();

			$imagem->move('assets/images/perfil', $filename);
			$object->imagem = $filename;
		}
		/***********************************/

		$object->perfil_pt = Input::get('perfil_pt');
		$object->perfil_en = Input::get('perfil_en');
		$object->metodologia_pt = Input::get('metodologia_pt');
		$object->metodologia_en = Input::get('metodologia_en');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto alterado com sucesso.');

		return Redirect::route('painel.perfil.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Perfil::find($id);
		//$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.perfil.index');
	}

}