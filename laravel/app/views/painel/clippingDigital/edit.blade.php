@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Clipping Digital
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.clippingDigital.update', $clipping->id), 'method' => 'put', 'id' => 'form-alter-clip-dig') ) }}

			<label>Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<input type="text" name="titulo_pt" required required-message="Informe o título do parceiro!" class="input-xxlarge" value="{{$clipping->titulo_pt}}">
			</label>

			<label>Título - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
				<input type="text" name="titulo_en" required required-message="Informe o título do parceiro!" class="input-xxlarge" value="{{$clipping->titulo_en}}">
			</label>
			
			<label>
				Data<br>
				<input type="text" name="data" class="datepicker" required required-message="Informe a data do Clipping!" value="{{date('d/m/Y', strtotime($clipping->data))}}">
			</label>

			<label>Descrição - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<textarea name="descricao_pt" class="completo pequeno">{{$clipping->descricao_pt}}</textarea>
			</label>

			<label>Descrição - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
				<textarea name="descricao_en" class="completo pequeno">{{$clipping->descricao_en}}</textarea>
			</label>

			<label>Link<br>
				<input type="text" name="link" class="input-xxlarge" required required-message="Informe o link de destino do Clipping!" value="{{$clipping->link}}">
			</label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop