<?php

return array(
	'perfil_paragrafo_1' => 'Escritório consolidado e reconhecido como um dos mais importantes do país, conta com 20 anos de atuação no mercado nacional e internacional, investindo em uma equipe motivada formada por arquitetos jovens e experientes, decoradores e designers de interiores que atuam nos diferentes núcleos da arquitetura e decoração de interiores, seguindo funções bem definidas e organogramas pré-estabelecidos.',
	'perfil_paragrafo_2' => 'Em busca de constantes desafios, a empresa dedica-se à elaboração de projetos focados na qualidade de vida por meio de soluções diferenciadas que transmitam conforto, segurança, bem-estar e sofisticação com criatividade, dinamismo e assertividade.',
	'perfil_paragrafo_3' => 'Os projetos são realizados com responsabilidade e total disponibilidade no atendimento ao cliente, e a produção ocorre de forma estruturada através da aplicação de metodologias de trabalho que seguem um planejamento baseado nas necessidades do cliente, respeito os cronogramas de atividades pré-definidos por ambas as partes e estabelecendo metas de trabalho dentro de um controle orçamentário que garante o melhor custo/benefício do produto final, bem como a excelência da qualidade dos serviços prestados. Desenvolve projetos de arquitetura e de interiores dentro dos segmentos comerciais, corporativos, residenciais e imobiliários presentes nas maiores cidades brasileiras em todas as regiões do país e exterior.',
	'metodologia_titulo' => 'METODOLOGIA',
	'metodologia_fidelidade_titulo' => 'FIDELIDADE',
	'metodologia_fidelidade_texto' => 'Atendemos de forma personalizada, estabelecendo um relacionamento transparente com o cliente, acompanheo-o em cada passo e etapa do processo.',
	'metodologia_viabilidade_titulo' => 'VIABILIDADE',
	'metodologia_viabilidade_texto' => 'Buscamos o equilíbrio entre as necessidades, desejos e a realidade do cliente, seguindo o planejamento de custos disponível por meio de orçamentos balanceados.',
	'metodologia_planejamento_titulo' => 'PLANEJAMENTO & COORDENAÇÃO',
	'metodologia_planejamento_texto' => 'Definimos todas as etapas do processo, estabelecendo metas, prazos e objetivos para cada uma delas junto ao cliente e fornecedores.',
	'metodologia_realizacao_titulo' => 'REALIZAÇÃO',
	'metodologia_realizacao_texto' => 'Atingimos os objetivos propostos através de um produto de reconhecimento pessoal ou comercial com plena satisfação do cliente.',
);