<?php

use \Cliente;

class ClientesController extends BaseController {

	protected $layout = 'frontend.template';

	public function index()
	{
		$this->layout->content = View::make('frontend.clientes')->with('clientes', Cliente::orderBy('ordem', 'ASC')->get())
																->with('imageDir', 'clientes');
	}

}