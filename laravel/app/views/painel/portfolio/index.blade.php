@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Portfolio <a href="{{ URL::route('painel.portfolio.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Projeto</a>
    	</h2>

        <div class="btn-group">
            @if($listaCategorias)
                @foreach ($listaCategorias as $key => $value) {
                    <a href="painel/portfolio?catId={{ $value->id }}" class="btn btn-mini @if($catId == $value->id) btn-warning @else btn-info @endif " style="float:none" title="Projetos de {{ $value->titulo_pt }}">{{ $value->titulo_pt }}</a>
                @endforeach
            @endif
        </div>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

            @if($catTit != false)
                <h2>Projetos de {{$catTit}}</h2>
            @else
                <h2>Todos os Projetos</h2>
            @endif

      		<table class="table table-bordered table-condensed table-sortable" data-tabela="projetos">

          		<thead>
            		<tr>
                        @if($catTit != false)
                            <th>Ordenar</th>
                        @endif
                        <th>Categoria</th>
                        <th>Título</th>
                        <th>Capa</th>
                        <th>Fotos do Projeto</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($portfolio as $projeto)

                	<tr class="tr-row" id="row_{{$projeto->id}}">
                        @if($catTit != false)
                            <td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
                        @endif
                        <td>{{$projeto->categoria->titulo_pt}}</td>
                        <td>
                            PT: {{ $projeto->titulo_pt }}
                            <br>
                            EN: {{ $projeto->titulo_en }}
                        </td>
                        <td><img src="assets/images/portfolio/thumbs/{{ $projeto->capa }}" style="max-width:140px;"></td>
                        <td>
                            <a href="{{ URL::route('painel.imagens.index', array('portfolio_id' => $projeto->id)) }}" class="btn btn-info">imagens</a>
                        </td>
                        <td class="crud-actions">
                            <a href="{{ URL::route('painel.portfolio.edit', $projeto->id ) }}" class="btn btn-primary">editar</a>

                            {{ Form::open(array('route' => array('painel.portfolio.destroy', $projeto->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.portfolio.destroy', $projeto->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
                            {{ Form::close() }}
                        </td>
                	</tr>
                  
            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop