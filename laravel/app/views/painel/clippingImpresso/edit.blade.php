@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Clipping Impresso
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.clippingImpresso.update', $clipping->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-clip-imp') ) }}

				<label>
					Título - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
					<input type="text" name="titulo_pt" required required-message="Informe o Título da publicação!" value="{{$clipping->titulo_pt}}">	
				</label>

				<label>
					Título - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
					<input type="text" name="titulo_en" required required-message="Informe o Título da publicação!" value="{{$clipping->titulo_en}}">	
				</label>

				<label>
					Data de Publicação<br>
					<input type="text" name="data" class="datepicker" required required-message="Informe a Data da publicação!" value="{{date('d/m/Y', strtotime($clipping->data))}}">
				</label>

				<label>Imagem<br>
				@if($clipping->capa)
					<img src="assets/images/clippings/thumbs/{{ $clipping->capa }}" style="width:200px;"><br>				
				@endif
				<input type="file" name="imagem"></label>

				<div class="form-actions">
		        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
		        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
		      	</div>
			{{ Form::close() }}

		</div>
	</div>

@stop