@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Perfil
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-bordered table-condensed">

          		<thead>
            		<tr>
                    <th>Imagem</th>
                    <th>Texto</th>
                    <th>Metodologia</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($perfil as $registro)

                	<tr class="tr-row">
                      <td rowspan="2"><img src="assets/images/perfil/{{ $registro->imagem }}" style="width:140px;"></td>
                      <td>
                        PT: {{ Str::words( strip_tags($registro->perfil_pt), 10 ) }}
                      </td>
                      <td>
                        PT: {{ Str::words( strip_tags($registro->metodologia_pt), 10 ) }}
                      </td>
                      <td rowspan="2">
                  		  <a href="{{ URL::route('painel.perfil.edit', $registro->id ) }}" class="btn btn-primary">editar</a>
                      </td>
                	</tr>
                  <tr>
                    <td>
                        EN: {{ Str::words( strip_tags($registro->perfil_en), 10 ) }}
                      </td>
                      <td>
                        EN: {{ Str::words( strip_tags($registro->metodologia_en), 10 ) }}
                      </td>
                  </tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop