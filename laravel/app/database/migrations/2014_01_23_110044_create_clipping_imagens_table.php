<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClippingImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clipping_impresso_imagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('clipping_impresso_id');
			$table->string('imagem');
			$table->integer('ordem')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clipping_impresso_imagens');
	}

}