<?php

return array(

	'titulo_site' => 'Debora Aguiar Arquitetos',
	'titulo_reduzido' => 'Debora Aguiar',

	'404' => 'Page not found',

	'nenhum_projeto' => 'No project found',
	'nenhum_clipping' => 'No clippings found',
	'nenhum_cliente' => 'No clients found',
	'nenhum_parceiro' => 'No partners found',

	'Foto' => 'Photo',
	'Thumbs Anteriores' => 'Next thumbnails',
	'Próximas Thumbs' => 'Previous thumbnails',
	
	'HOME PROJETOS' => 'PROJECTS HOME',
	'PROJETO ANTERIOR' => 'PREVIOUS PROJECT',
	'PRÓXIMO PROJETO' => 'NEXT PROJECT',
	'FORNECEDORES' => 'FORNECEDORES'
);