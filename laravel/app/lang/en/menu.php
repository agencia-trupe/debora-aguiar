<?php

return array(

	'home_titulo' => 'Home Page',
	'home' => 'HOME',

	'perfil_titulo' => 'Profile',
	'perfil' => 'PROFILE',

	'portfolio_titulo' => 'Portfolio',
	'portfolio' => 'PORTFOLIO',

	'clientes_titulo' => 'Our Clients',
	'clientes' => 'CLIENTS',

	'parceiros_titulo' => 'Our Partners',
	'parceiros' => 'PARTNERS',

	'clipping_titulo' => 'Clipping',
	'clipping' => 'CLIPPING',

	'contato_titulo' => 'Contact',
	'contato' => 'CONTACT',

	'area_restrita_titulo' => 'área restrita',
	'area_restrita' => 'área restrita',

	'versao_ingles_titulo' => 'english version',
	'versao_ingles' => 'english',
	'versao_portugues_titulo' => 'versão em português',
	'versao_portugues' => 'português',

	'assinatura' => 'Debora Aguiar &bull; All rights reserved',
	'criacao_sites_titulo' => 'Site Creation : Trupe Agência Criativa',
	'criacao_sites' => 'Site Creation',
	'trupe' => 'Trupe Agência Criativa',
	
	'clipping_impresso_titulo' => 'Clippings Impressos',
	'clipping_impresso' => 'IMPRESSO',
	'clipping_digital_titulo' => 'Clippings Digitais',
	'clipping_digital' => 'DIGITAL',

	'toggle' => 'Show/Hide Menu'
);