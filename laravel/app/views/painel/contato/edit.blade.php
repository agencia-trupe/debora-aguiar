@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Contato
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.contato.update', $contato->id), 'method' => 'put', 'id' => 'form-edit-contato') ) }}

			<label>
				Descrição - Versão em Português <img src="blank.gif" class="flag flag-br" ><br>
				<textarea name="texto_pt" class="pequeno completo">{{$contato->texto_pt}}</textarea>
			</label>

			<label>
				Descrição - Versão em Inglês <img src="blank.gif" class="flag flag-us"><br>
				<textarea name="texto_en" class="pequeno completo">{{$contato->texto_en}}</textarea>
			</label>


			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop