<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClippingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clipping_impresso', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo_pt')->nullable();
			$table->string('titulo_en')->nullable();
			$table->string('slug_pt');
			$table->string('slug_en');
			$table->date('data');
			$table->string('capa');
			$table->integer('ordem')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clipping_impresso');
	}

}