<?php

class ProjetosCategoriasSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'titulo_pt' => 'ARQUITETURA',
				'titulo_en' => 'ARCHITECTURE',
				'slug_pt' => 'arquitetura',
				'slug_en' => 'architecture',
				'ordem' => '0'
            ],
            [
            	'titulo_pt' => 'INTERIORES RESIDENCIAL',
				'titulo_en' => 'RESIDENTIAL INTERIOR',
				'slug_pt' => 'interiores-residencial',
				'slug_en' => 'residential-interior',
				'ordem' => '1'
            ],
            [
            	'titulo_pt' => 'INCORPORAÇÃO',
				'titulo_en' => 'INCORPORATION',
				'slug_pt' => 'incorporacao',
				'slug_en' => 'incorporation',
				'ordem' => '2'
            ],
            [
            	'titulo_pt' => 'COMERCIAL',
				'titulo_en' => 'COMMERCIAL',
				'slug_pt' => 'comercial',
				'slug_en' => 'commercial',
				'ordem' => '3'
            ],
            [
            	'titulo_pt' => 'CORPORATIVO',
				'titulo_en' => 'CORPORATE',
				'slug_pt' => 'corporativo',
				'slug_en' => 'corporate',
				'ordem' => '4'
            ],
            [
            	'titulo_pt' => 'MOSTRAS',
				'titulo_en' => 'SHOWS',
				'slug_pt' => 'mostras',
				'slug_en' => 'shows',
				'ordem' => '5'
            ],
            [
            	'titulo_pt' => 'GIRO 360',
				'titulo_en' => '360 SPIN',
				'slug_pt' => 'giro-360',
				'slug_en' => '360-spin',
				'ordem' => '6'
            ]
        ];

        DB::table('projetos_categorias')->delete();
        DB::table('projetos_categorias')->insert($data);
    }

}